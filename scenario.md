# Git intensive 

## Какие команды показываем?

Основные команды из презентации:
- [x] commit
- [x] push
- [x] checkout
- [x] merge
- [x] fetch
- [ ] clone
- [ ] pull

## Сценарий

- [x] Создание локального репозитория
- [x] Публикация в GitLab
- [x] Merge

### 1. Создание локального репозитория

- PyCharm
  - Открыть проект git_interactive
  - VCS -> Create Git Repository...
  - First Commit: .idea -> .gitignore

### 2. Публикация в GitLab

- https://gitlab.com/
- git_interactive
- IT Лекторий Itentika. Git intensive 27.07.22

